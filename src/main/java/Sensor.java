import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

import org.json.JSONObject;

public class Sensor {

    private final static String QUEUE_NAME = "Activity_Queue";
    private final static String HOST = "roedeer.rmq.cloudamqp.com";
    private final static String FILE_PATH = "F:\\Altele\\Facultate\\An4_Sem1\\SD\\Proiect\\medication-platform-sensor-simulator\\src\\main\\resources\\activity.txt";

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(HOST);
        factory.setVirtualHost("aqpxjuxx");
        factory.setUsername("aqpxjuxx");
        factory.setPassword("DYD3dDYScYZUA0O0eZTlRGTKyfC-LDkL");

        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);

            File activityFile = new File(FILE_PATH);
            Scanner myReader = new Scanner(activityFile);
            while (myReader.hasNextLine()) {
                String message = myReader.nextLine();
                String[] messageParts = message.trim().split("\\s\\s+");
                if(messageParts.length > 2)
                {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("patientId", "7220d482-5b5b-4fe7-ac75-dd49ad75e730");
                    jsonObject.put("activity", messageParts[2]);
                    jsonObject.put("startDate", messageParts[0]);
                    jsonObject.put("endDate", messageParts[1]);
                    channel.basicPublish("", QUEUE_NAME, null, jsonObject.toString().getBytes(StandardCharsets.UTF_8));
                    System.out.println("Sent: " + message);
                    Thread.sleep(1000);
                }
            }
        }
    }
}
